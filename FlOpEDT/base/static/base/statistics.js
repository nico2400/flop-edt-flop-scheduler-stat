// This file is part of the FlOpEDT/FlOpScheduler project.
// Copyright (c) 2017
// Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.
// 
// You can be released from the requirements of the license by purchasing
// a commercial license. Buying such a license is mandatory as soon as
// you develop activities involving the FlOpEDT/FlOpScheduler software
// without disclosing the source code of your own applications.

/* global array of object depending on the available option */

var room_datas;
var teacher_datas;
var group_datas;

var existing_options = ["room_activity", "tutor_hours"];
var available_options = {} ;
      for(let i = 0; i<existing_options.length; i++){
        available_options[existing_options[i]] = {
          graph_title : "Default",
          type: 'Default',
          text_yaxis : 'Default',
          series_name : 'Default',
          series_data : [["default", 0]],
          tooltip : "Default" 
      }
    }

/* graph drawing */

function display_graph(){
        

        let option_selected = document.getElementById('select_statistic').value;
        console.log('Running display_graph');
        console.log(option_selected);
       /*we need to add a new case depending on the number of new options that we add in the html select clause.
         data will be read from an object that is in the statistics.js file*/
        state();

        var graph_datas = available_options[option_selected];
        console.log("graph_datas before : ", graph_datas);

        Highcharts.chart('graphique', {
          chart: {
            type: 'column',
          },
          title: {
            text: graph_datas.graph_title,
            style: {
            "font-family": "'Open Sans', sans-serif", // font of the title
            "color": "#373737", // color of the title
            "fontSize": "24px" // size of the title
            }
          },
          xAxis: {
            type: 'category',
            labels: {
              rotation: -45,
              style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
              }
            }
          },
          yAxis: {
            min: 0, // min of the y axis
            max: 80, // max of the y axis
            title: {
              text: graph_datas.text_yaxis
            }
          },

          tooltip: {
            pointFormat: graph_datas.tooltip + "<b>{point.y:.1f}</b>" // hint displayed when pointing a bar
          },
          series: [{
            name: graph_datas.series_name,
            data: graph_datas.series_data,
            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
          }]
        })
}
  

/*
    HELPER : remove child nodes
*/
function removeChildNodes(parentNode) {
  if (parentNode) {
    while (parentNode.hasChildNodes()) {
      parentNode.removeChild(parentNode.firstChild);
    }
  }
}


/*
    Display the department's tutor list with 
    their total number of hours of activity
*/
function displayTutorHours(data) {
  let container = d3.select('#statistics table');

  // Clear table    
  removeChildNodes(container.node());

  // Insert header 
  header = container.append('tr');
  header.append('th').text('Professeur');
  header.append('th').text('Nombre de cours');

  tutor = container.selectAll('tr')
    .data(data)
    .enter()
    .append('tr');

  // Tutor name
  tutor
    .append('td')
    .text((d) => d[1]);

  // Number of slots
  tutor
    .append('td')
    .text((d) => d[4]);

    // give a array to the global var of room datas
  teacher_datas = data; 
  console.log(teacher_datas);
  init_options();
}

function displayGroupHours(data){
  let container = d3.select('#statistics table');

  // Clear table
  removeChildNodes(container.node())

  // Insert header
  header = container.append('tr');
  header.append('th').text('Groupe');
  header.append('th').text('Promo');
  header.append('th').text('Nombre de cours');

  group = container.selectAll('tr')
    .data(data)
    .enter()
    .append('tr')

  // Group name
  group
    .append('td')
    .text((d) => d[1]);

  //group year
  group
    .append('td')
    .text((d)=> d[2])
  // Number of course
  group
    .append('td')
    .text((d) => d[3]);

  group_datas = data; 
  console.log("group datas : ", group_datas);
  init_options();

}

/*
    Display the number of days of inactivity for each room
*/
function displayRoomActivity(data) {

  let container = d3.select('#statistics table');

  // Clear table
  removeChildNodes(container.node());

  // Insert header 
  header = container.append('tr');
  header.append('th').text(gettext('Room'));
  header.append('th').text(gettext('Number of days'));

  rooms = container.selectAll('tr')
    .data(data.room_activity)
    .enter()
    .append('tr');

  // Room name
  rooms
    .append('td')
    .text((d) => d.room);

  // Number of days of inactivity
  rooms
    .append('td')
    .text((d) => d.count);

    // give a array to the global var of room datas
  room_datas = data; 
  console.log(room_datas)
  init_options();
}

/*
    Global method to display 
*/
function displayStatistic(label) {
  let statistic = available_statistics[label];
  if (statistic) {
    // TODO : loading

    // Request server to get datas
    fetchStatistcs(statistic.url, statistic.callback);
  }
}

function changeStatisticEventHandler(event) {
  displayStatistic(event.target.value);
}

/*
    Global method to request statistics 
*/
function fetchStatistcs(url, callback) {
  show_loader(true);
  $.ajax({
    type: "GET",
    dataType: 'json',
    url: url,
    async: true,
    contentType: "text/json",
    success: function (value) {
      callback(value);
    },
    error: function (xhr, error) {
      console.log(xhr);
      console.log(error);
    },
    complete: function () {
      show_loader(false);
    }
  });
}

/*
    Initialization
*/
function init() {
  statisticContainer = document.querySelector('#statistics');

  statisticSelect = document.querySelector('#select_statistic');
  statisticSelect.onchange = changeStatisticEventHandler;

  displayStatistic(statisticSelect.options[statisticSelect.selectedIndex].value);

}

// must be only call by init_options()
/////////////////////////////////////////////////////////////////////////////////
        function getArrayRoom(data){
          console.log("before room array : ");
          console.log(data);

          let newTab = Array();
          for(var i = 0; i<data.room_activity.length; i++){
            newTab.push([data.room_activity[i].room, data.room_activity[i].count]);
          }

          console.log("after room array : ");
          console.log(newTab);
          return newTab;
        }

        function getArrayTeacher(data){
          console.log("before Teacher array : ");
          console.log(data);

          let newTab = Array();
          for(var i = 0; i<data.length; i++){
            newTab.push([data[i][1], data[i][4]]);
          }

          console.log("after Teacher array : ");
          console.log(newTab);
          return newTab;
        }

        function getArrayGroup(data){
          console.log("before Group array : ");
          console.log(data);

          let newTab = Array();
          for(var i = 0; i<data.length; i++){
            newTab.push([data[i][1], data[i][3]]);
          }

          console.log("after Group array : ");
          console.log(newTab);
          return newTab;
        }

/////////////////////////////////////////////////////////////////////////////////

function init_options(){

    console.log("INIIIIIIIIT");
    console.log(room_datas);
    console.log(room_datas===undefined);
    console.log(room_datas==='undefined');
    console.log(!(room_datas===undefined));
    console.log(!room_datas==='undefined');


  //stay written in "hard" for the moment, we must add a new case for each new options
  console.log(!room_datas===undefined);
   console.log(!teacher_datas===undefined);

  if(room_datas!==undefined){
    available_options["room_activity"] = {
      
          graph_title : "jours d'innocupations par salles",
          type: 'Salles',
          text_yaxis : 'Nombre de jours',
          series_name : 'Salles',
          series_data : getArrayRoom(room_datas),
          tooltip : "Number of courses"
      }
    if(document.getElementById('select_statistic').value=="room_activity"){
      display_graph();
    }  
  }

  if(teacher_datas!==undefined){
    console.log('Inside init_options')
    console.log(teacher_datas);
    available_options["tutor_hours"] = {
          graph_title : "nombre d'heures par professeurs",
          type: 'Professeurs',
          text_yaxis : 'Nombre de cours',
          series_name : 'Professeurs',
          series_data : getArrayTeacher(teacher_datas),
          tooltip : "Number of hours"
      }
    if(document.getElementById('select_statistic').value=="tutor_hours"){
      display_graph();
    }  
      
  }

  if(group_datas!==undefined){
    console.log('Inside init_options')
    console.log(group_datas);
    available_options["group_hours"] = {
          graph_title : "Nombre de cours par groupe",
          type: 'Groupes',
          text_yaxis : 'Nombre de cours',
          series_name : 'Groupes',
          series_data : getArrayGroup(group_datas),
          tooltip : "Nombres de cours"
      }
    if(document.getElementById('select_statistic').value=="group_hours"){
      display_graph();
    }  
      
  }




}

function state(){
  console.log("room", room_datas);

  console.log("teacher", teacher_datas);

  console.log("options ", available_options);
}

/*
    Main process
*/
var statisticContainer;
var statisticSelect;




var available_statistics = {
  'room_activity': {
    url: statistics_urls['room_activity'],
    callback: displayRoomActivity,
  },
  'tutor_hours': {
    url: statistics_urls['tutor_hours'],
    callback: displayTutorHours,
  },
  'group_hours': {
    url: statistics_urls['group_hours'],
    callback: displayGroupHours,
  }
};



document.addEventListener('DOMContentLoaded', init);


